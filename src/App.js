import logo from './assets/logo.svg';
import style from "./App.module.css";
import {Component} from "react";
import Timer from "./v1/Timer";
import TimersTable from "./v1/TimersTable";
import {PomoTimerV2} from "./v2/PomoTimerV2";
import {PomoTimerV3} from "./v3/PomoTimerV3";

export const colors = {
  bg: '#d6d4ba',
  strong: '',
  text: '#474646',
  over: '#8f4727'
}

export const styles = {
  main: {backgroundColor: colors.bg},
  array: {},
  button: {},
}

export const timerStates = {
    NOT_STARTED: 1,
    RUNNING: 2,
    PAUSED: 3,
}

export const phases = {
    FOCUS: 1,
    SHORT_BREAK : 2,
    LONG_BREAK: 3
}

export const timerLengths = {
    // focus: 20*60,
    // shortBreak: 5*60,
    // longBreak: 15*60,
    focus: 3,
    shortBreak: 1,
    longBreak: 2,
}

const LONG_BREAK_FOCUS_ITERATION = 4;

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            timerRunning: false,
            phase: phases.FOCUS,
            focusIteration: 1,
            logoClassName: style.logo,
            version: 3,
            timers: [],
            focusTime: timerLengths.focus,
            shortBreakTime: timerLengths.shortBreak,
            longBreakTime: timerLengths.longBreak
        }
    }

    /*   V1   */

    handleTimerStartedV1 = () => {
        console.log('handle timer started');
        this.setState(() => {
            return {
                timerStarted: true,
                logoClassName: style.spinningLogo,
            }
        });
    }

    handleTimerStoppedV1 = (timeCount) => {
        console.log('handle timer stopped');
        this.setState((state) => {
            return {
                timerStarted: false,
                logoClassName: style.logo,
                timers: [...state.timers, {date: new Date(), count: timeCount}],
            }
        });
    }

    /*   V2   */

    handleTimerStartedV2 = () => {
        console.log('handle timer started V2');
        this.setState(() => {
           return {
               timerStarted: true,
               logoClassName: style.spinningLogo,
           }
        });
    }

    handleTimerStoppedV2 = () => {
        console.log('handle timer stopped V2');
        this.setState((state) => {
            return {
                timerStarted: false,
                logoClassName: style.logo,
            }
        });
    }

    handleTimerFinished = () => {
        console.log('handle timer finished');
        this.setState((state) => {
            const phaseAndIteration = this.getNextPhaseAndIteration(state);
            return {
                phase: phaseAndIteration[0],
                focusIteration: phaseAndIteration[1],
                logoClassName: style.logo,
            }
        });
    }

    getNextPhaseAndIteration = (state) => {
        if (state.phase === phases.FOCUS) {
            if (state.focusIteration < LONG_BREAK_FOCUS_ITERATION) {
                return [phases.SHORT_BREAK, state.focusIteration];
            }
            return [phases.LONG_BREAK, 0];
        }
        return [phases.FOCUS, state.focusIteration + 1];
    }

    displayCurrentPhase = () => {
        if (this.state.phase === phases.FOCUS) {
            return "Concentration";
        } else if (this.state.phase === phases.SHORT_BREAK) {
            return "Petite pause";
        } else if (this.state.phase === phases.LONG_BREAK) {
            return "Grande pause";
        }
    }

    updateVersion = (version) => {
        console.log('version change');
        this.setState( () => {
           return {
               timerRunning: false,
               phase: phases.FOCUS,
               focusIteration: 1,
               logoClassName: style.logo,
               version: version,
               timers: [],
               focusTime: timerLengths.focus,
               shortBreakTime: timerLengths.shortBreak,
               longBreakTime: timerLengths.longBreak
           }
        });
    }

    log = () => {
        console.log(this.state);
        console.log(this.props);
    }

    render() {
        if (this.state.version === 1) {
            return (
                <div className={style.container}>
                    <header className={style.mainTitle}>
                        <h2>Pomodoro Timer V1</h2>
                    </header>
                    <section>
                        <img src={logo} className={this.state.logoClassName} alt="logo"/>
                    </section>

                    <Timer timerStarted={ this.handleTimerStartedV1 } publishTime={ (timeCount) => this.handleTimerStoppedV1(timeCount) }></Timer>

                    <TimersTable timers={ this.state.timers }></TimersTable>

                    <section className={style.footer}>
                        <button onClick={ () => this.updateVersion(2) } className={ style.button }>Go to version 2</button>
                        <button onClick={ () => this.updateVersion(3) } className={ style.button }>Go to version 3</button>
                        <button onClick={ this.log } style={ styles.button }>Debug</button>
                        <a href="https://reactjs.org" target="_blank" rel="noopener noreferrer">Docs reacts</a>
                    </section>
                </div>
            );
        } else if (this.state.version === 2) {
            return (
                <div className={style.container}>
                    <header className={style.mainTitle}>
                        <h2>Pomodoro Timer V2</h2>
                    </header>
                    <section className={style.lowTitle}>
                        <img src={logo} className={this.state.logoClassName} alt="logo"/>
                        <h3>{ this.displayCurrentPhase()}</h3>
                    </section>

                    <PomoTimerV2
                        timerStarted={ this.handleTimerStartedV2 }
                        timerStopped={ this.handleTimerStoppedV2 }
                        timerFinished={ this.handleTimerFinished }
                        focusTime={ this.state.focusTime }
                        shortBreakTime={ this.state.shortBreakTime }
                        longBreakTime={ this.state.longBreakTime }
                        currentPhase={ this.state.phase }>
                    </PomoTimerV2>

                    <section className={style.footer}>
                        <button onClick={ () => this.updateVersion(1) } className={ style.button }>Go to version 1</button>
                        <button onClick={ () => this.updateVersion(3) } className={ style.button }>Go to version 3</button>
                        <button onClick={ this.log } style={ styles.button }>Debug</button>
                        <a href="https://reactjs.org" target="_blank" rel="noopener noreferrer">Docs reacts</a>
                    </section>
                </div>
            );
        } else if (this.state.version === 3) {
            return (
                <div className="bg-sky-700 mx-auto px-4 py-2 column-1 min-h-screen ">
                    <header className="grid grid-cols-2 mx-auto">
                        <h2 className="mx-auto">Pomodoro Timer V3</h2>
                        <div className="mx-auto">Reglages</div>
                    </header>
                    <section className="grid grid-cols-1 grid-rows-2 my-10">
                        <img src={logo} className={`${this.state.logoClassName} mx-auto`} alt="logo"/>
                        <h3 className="mx-auto my-auto">{ this.displayCurrentPhase()}</h3>
                    </section>

                    <PomoTimerV3
                        timerStarted={ this.handleTimerStartedV2 }
                        timerStopped={ this.handleTimerStoppedV2 }
                        timerFinished={ this.handleTimerFinished }
                        focusTime={ this.state.focusTime }
                        shortBreakTime={ this.state.shortBreakTime }
                        longBreakTime={ this.state.longBreakTime }
                        currentPhase={ this.state.phase }>
                    </PomoTimerV3>

                    <section className="flex sm:justify-center space-x-4 my-10">
                        {[
                            ['Go to version 1', () => this.updateVersion(1)],
                            ['Go to version 2', () => this.updateVersion(2)],
                            ['Debug', this.log],
                        ].map(([title, onClick]) => (
                            <button onClick={onClick} className="rounded-lg px-3 py-2 text-slate-700 font-medium hover:bg-slate-600 hover:text-slate-900">{title}</button>
                        ))}
                        <a href="https://reactjs.org" target="_blank" className="rounded-lg px-3 py-2 text-slate-700 font-medium hover:bg-slate-100 hover:text-slate-900" rel="noopener noreferrer">Docs reacts</a>
                    </section>
                </div>
            );
        }
    }
}

export default App;
