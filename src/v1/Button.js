import {Component} from "react";
import style from './Button.module.css';

class Button extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isStarted: false,
        }
    }

    handleClick = () => {
        this.setState((state) => {
            return {
                isStarted: !state.isStarted,
            }
        });
        this.props.onClick();
    }

    render() {
        return (
            <p
                onClick={ this.handleClick }
                // className={style.button}
                className={`${style['clockBtn']} ${style[`clockBtn${ this.state.isStarted ? 'Stop' : 'Start' }`]}`}
            >
                { this.state.isStarted ? 'Stop' : 'Start' }
            </p>
        )
    }
}

export default Button;