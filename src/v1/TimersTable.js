import {Component} from "react";
import style from './TimersTable.module.css';

class TimersTable extends Component {

    render() {
        return (
            <table className={ style.timersTable }>
                <thead><tr><th>Date</th><th>Time</th></tr></thead>
                <tbody>
                {this.props.timers.map(timer => {
                    return (
                        <tr key={ timer.date.getMilliseconds() }>
                            <td>{ timer.date.toLocaleDateString() } at { timer.date.toLocaleTimeString() }</td>
                            <td>{ timer.count }</td>
                        </tr>
                    )
                })}
                </tbody>
            </table>
        )
    }
}

export default TimersTable;