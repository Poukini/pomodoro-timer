import {Component} from "react";
import style from './Timer.module.css'
import Button from "./Button";

const zeroPad = (num, places) => {
    return String(num).padStart(places, '0');
}

class Timer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            started: false,
            value: 0
        }
    }

    handleButtonClick = () => {
        this.state.started ? this.stopClock() : this.startClock();
    }

    startClock = () => {
        console.log('start clock');
        this.props.timerStarted();
        this.timerId = setInterval(() => {
            this.setState((state) => {
                return {
                    started: true,
                    value: state.value + 1
                };
            })
        }, 10);
    }

    stopClock = () => {
        console.log('stop clock');
        this.props.publishTime(this.state.value);
        this.setState( () => {
            return {
                started: false,
                value: 0
            }
        });
        clearInterval(this.timerId);

    }

    displayTimer = (timerCount) => {
        return zeroPad(Math.floor(timerCount / 3600), 2)
            + ':' + zeroPad(Math.floor(timerCount / 60) %60, 2)
            + ':' + zeroPad(timerCount % 60, 2);
    }

    render() {
        return (
            <>
                <p className={style.clockTimer}>{ this.displayTimer(this.state.value) }</p>
                <Button onClick={ this.handleButtonClick } />
            </>
    )
    }

}

export default Timer;
