import {Component} from "react";
import { Helmet } from 'react-helmet';
import {phases, timerStates} from "../App";
import style from './PomoTimerV2.module.css';

const zeroPad = (num, places) => {
    return String(num).padStart(places, '0');
}

export class PomoTimerV2 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            timerState: timerStates.NOT_STARTED,
            value: this.getCurrentTimerDuration(),
        }
    }

    getCurrentTimerDuration = () => {
        if (this.props.currentPhase === phases.FOCUS) {
            return this.props.focusTime;
        }
        if (this.props.currentPhase === phases.SHORT_BREAK) {
            return this.props.shortBreakTime;
        }
        if (this.props.currentPhase === phases.LONG_BREAK) {
            return this.props.longBreakTime;
        }
    }

    handleButtonClick = () => {
        if (this.state.timerState === timerStates.RUNNING) {
            this.stopClock();
        } else if (this.state.timerState === timerStates.PAUSED) {
            this.resumeClock();
        } else if (this.state.timerState === timerStates.NOT_STARTED) {
            this.startClock();
        }
    }

    resumeClock = () => {
        console.log('resume clock');
        this.props.timerStarted();
        this.setState(() => {
            return {
                timerState: timerStates.RUNNING,
            };
        })
        this.timerId = this.setupTimerDecrementInterval();
    }

    setupTimerDecrementInterval() {
        return setInterval(() => {
            if (this.state.value === 0) {
                return this.finishedClock();
            }
            this.setState((state) => {
                return {
                    value: state.value - 1,
                };
            })
        }, 1000);
    }

    startClock = () => {
        console.log('start clock');
        this.props.timerStarted();
        this.setState(() => {
            return {
                timerState: timerStates.RUNNING,
                value: this.getCurrentTimerDuration(),
            };
        })
        this.timerId = this.setupTimerDecrementInterval();
    }

    stopClock = () => {
        console.log('stop clock');
        this.props.timerStopped();
        this.setState( () => {
            return {
                timerState: timerStates.PAUSED
            }
        });
        clearInterval(this.timerId);
    }

    finishedClock = () => {
        console.log('finished clock');
        this.props.timerFinished();
        this.setState(() => {
           return {
               timerState: timerStates.NOT_STARTED,
           }
        });
        clearInterval(this.timerId);
    }

    displayTimer = (timerCount) => {
        const hourPrefix = timerCount > 3600 ? zeroPad(Math.floor(timerCount / 3600), 2) + ':' : '';
        return hourPrefix + zeroPad(Math.floor(timerCount / 60) %60, 2)
            + ':' + zeroPad(timerCount % 60, 2);
    }

    log = () => {
        console.log(this.state);
        console.log(this.props);
    }

    render() {
        const timerDisplayValue = this.displayTimer(this.state.value);
        const buttonText = this.state.timerState === timerStates.RUNNING ? 'Stop'
            : this.state.timerState === timerStates.PAUSED ? 'Resume' : 'Start';
        return (
            <>
                <Helmet>
                    <title>{ timerDisplayValue }</title>
                </Helmet>
                <p className={style.clockTimer}>{ timerDisplayValue }</p>
                <p
                    onClick={ this.handleButtonClick }
                    // className={style.button}
                    className={`${style['clockBtn']} ${style[`clockBtn${ this.state.timerState === timerStates.RUNNING ? 'Stop' : 'Start' }`]}`}
                >
                    { buttonText }
                </p>
                <button onClick={ this.log }>Debug</button>
            </>
        )
    }

}